#!/bin/bash

# Getting the current date
date=$(date)

# The last 10 users who logged in
last_users=$(last -n 10)

# Available swap space
swap_space=$(free -m | awk '/Swap/ {print $2}')

# The kernel version
kernel_version=$(uname -r)

# The IP address
ip_address=$(hostname -I)

# Outputing the information to serverinfo.info
echo "Date: $date" > /tmp/serverinfo.info
echo "Last 10 users: $last_users" >> /tmp/serverinfo.info
echo "Swap space: $swap_space MB" >> /tmp/serverinfo.info
echo "Kernel version: $kernel_version" >> /tmp/serverinfo.info
echo "IP address: $ip_address" >> /tmp/serverinfo.info

